package isel.poo.mines.view;

import isel.poo.console.Window;
import isel.poo.mines.model.*;
import isel.poo.console.tile.*;

public class MinesView implements GameListener {
    private final Window win;
    private final Game game;
    private TilePanel panel;
    private MineView mine = new MineView();
    private NumberView field;

    public MinesView(Game game) {
        this.game = game;
        int lines = game.getLines(), cols = game.getCols();
        win = new Window("Mines",lines+4,cols+12);
        panel = new TilePanel(15,20, 1);
        panel.center(19,25);
        panel.repaint();
        repaint();
    }

    private void repaint() {
        for (Actor actor : game) {
            Position pos = actor.getPosition();
            Tile t = (actor instanceof Mine) ? new MineView() : new NumberView();
            panel.setTile(pos.line, pos.col, t);
        }
    }

    public void close() {
        win.message("Bye");
        win.close();
    }

    public boolean newGame() {
        return win.question("New game");
    }

    @Override
    public void gameOver(boolean winner) { }
}
