package isel.poo.mines.view;

import isel.leic.pg.Console;
import isel.poo.console.tile.Tile;

public class MineView extends Tile {
    @Override
    public void paint() {
        Console.color(Console.WHITE,Console.DARK_GRAY);
        print(0,0,"*");
    }
}
