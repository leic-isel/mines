package isel.poo.mines.view;

import isel.leic.pg.Console;
import isel.poo.console.tile.Tile;

public class NumberView extends Tile {

    public String id = "1";

    @Override
    public void paint() {
        Console.color(Console.BLACK, Console.WHITE);
        print(0,0, id);
    }
}
