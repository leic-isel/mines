package isel.poo.mines.model;

public interface GameListener {
    //void actorMoved(Actor actor, Position old);
    //void actorRemoved(Position pos);
    //void actorCreated(Actor actor);
    //void actorChanged(Actor actor);
    void gameOver(boolean winner);
}
