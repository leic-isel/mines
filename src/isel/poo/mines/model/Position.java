package isel.poo.mines.model;

public class Position {
    public final int line;
    public final int col;

    public Position(int line, int col) { this.line = line; this.col = col; }

    private static Position[][] all;
    static void init(int lines, int cols) {
        all = new Position[lines][cols];
    }

    public static Position random() {
        return of((int)(Math.random()*getLines()),(int)(Math.random()*getCols()));
    }

    private static Position of(int l, int c) {
        Position pos = all[l][c];
        if (pos==null)
            all[l][c] = pos = new Position(l, c);
        return pos;
    }

    public static int getLines() {
        return all.length;
    }

    public static int getCols() {
        return all[0].length;
    }
}
