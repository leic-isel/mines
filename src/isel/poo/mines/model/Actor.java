package isel.poo.mines.model;

public class Actor {
    Position position;
    Game game;

    public Actor(Position position, Game game) {
        this.position = position;
        this.game = game;
        game.field.set(this);
    }

    public Position getPosition() { return position; }
}
