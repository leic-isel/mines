package isel.poo.mines.model;

import java.util.Iterator;

public class Game implements Iterable<Actor> {
    final int NUM_MINES = 3;
    Field field;
    Mine mine;
    private GameListener[] listeners;

    public Game(int lines, int cols) {
        Position.init(lines,cols);
        field = new Field(lines, cols);
        createMines();
    }

    private void createMines() {
        for (int i = 0; i < NUM_MINES; i++) {
            Position randomPos = field.randomFreePosition();
            new Mine(randomPos, this);
            CreateNumbersAroundMines(randomPos);
        }
    }

    private void CreateNumbersAroundMines(Position pos) {
         // aqui, tem de fazer o get do Actor [x,y] da posição line e col +1 e incrementar o id

        int line = pos.line, col = pos.col;

        if(field.getActor(line-1, col-1) == null){
            Position newPosition = new Position(line-1, col-1);
            new Number(newPosition,this);
        }
        if(field.getActor(line-1, col) == null){
            Position newPosition = new Position(line-1, col);
            new Number(newPosition,this);
        }
        if(field.getActor(line-1, col+1) == null){
            Position newPosition = new Position(line-1, col+1);
            new Number(newPosition,this);
        }
        if(field.getActor(line, col-1) == null){
            Position newPosition = new Position(line, col-1);
            new Number(newPosition,this);
        }
        if(field.getActor(line, col+1) == null){
            Position newPosition = new Position(line, col+1);
            new Number(newPosition,this);
        }
        if(field.getActor(line+1, col-1) == null){
            Position newPosition = new Position(line+1, col-1);
            new Number(newPosition,this);
        }
        if(field.getActor(line+1, col) == null){
            Position newPosition = new Position(line+1, col);
            new Number(newPosition,this);
        }
        if(field.getActor(line+1, col+1) == null){
            Position newPosition = new Position(line+1, col+1);
            new Number(newPosition,this);
        }
    }

    public int getLines() { return Position.getLines(); }
    public int getCols() { return Position.getCols(); }

/*    public void selectedActor(Actor actor) {
        for (GameListener listener : listeners)
            listener.actorChanged(actor);
    }*/

    @Override
    public Iterator<Actor> iterator() { return field.iterator(); }
}
