package isel.poo.mines;

import isel.leic.pg.Console;
import isel.poo.mines.model.Game;
import isel.poo.mines.view.MinesView;

import java.awt.event.KeyEvent;

public class Main {
    private static final int LINES=15, COLS=20;
    private Game game = new Game(LINES, COLS);
    private MinesView view = new MinesView(game);

    public static void main(String[] args) { new Main().run(); }

    private void run() {
        for(;;) {
            int key = Console.waitKeyPressed(1000);
            if (key== KeyEvent.VK_ESCAPE) break;
            if (key!=Console.NO_KEY) {
                //processKey(key);
                Console.waitKeyReleased(key);
                if(!view.newGame()) break;
                    //view.reset();
                //else break;
            }
        }
        view.close();
    }
}

